<?php
/**
 * @file
 * Views handler.
 */

/**
 * A handler to provide proper displays for dates.
 */
class fb_feed_views_handler_field_picture extends views_handler_field {

  /**
   * Define option_definition function.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['image_style'] = array('default' => '');

    return $options;
  }

  /**
   * Define options_form function.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    if (module_exists('image')) {
      $styles = image_styles();
      $style_options = array('' => t('Default'));
      foreach ($styles as $style) {
        $style_options[$style['name']] = $style['name'];
      }

      $form['image_style'] = array(
        '#title' => t('Image style'),
        '#description' => t('Using <em>Default</em> will use the site-wide image style for user pictures set in the <a href="!account-settings">Account settings</a>.', array('!account-settings' => url('admin/config/people/accounts', array('fragment' => 'edit-personalization')))),
        '#type' => 'select',
        '#options' => $style_options,
        '#default_value' => $this->options['image_style'],
      );
    }
  }

  /**
   * Define render function.
   */
  function render($values) {
    if ($this->options['image_style'] && module_exists('image')) {
      $picture_url = $this->get_value($values);
      if (!empty($picture_url)) {
        $output = theme('imagecache_external', array('style_name' => $this->options['image_style'], 'path' => $picture_url));
      }
      else {
        $output = '';
      }
    }

    return $output;
  }
}
