<?php
/**
 * @file
 * Main file for fb_feed module.
 */

/**
 * Implements hook_permission().
 */
function fb_feed_permission() {
  $permissions = array(
    'create fb_feed entities' => array(
      'title' => t('Create fb_feed entity'),
      'description' => t('Allows users to create fb_feed entity.'),
      'restrict access' => TRUE,
    ),
    'view fb_feed entities' => array(
      'title' => t('View fb_feed entity'),
      'description' => t('Allows users to view fb_feed entity.'),
      'restrict access' => TRUE,
    ),
    'edit any fb_feed entities' => array(
      'title' => t('Edit any fb_feed entity'),
      'description' => t('Allows users to edit any fb_feed entity.'),
      'restrict access' => TRUE,
    ),
    'edit own fb_feed entities' => array(
      'title' => t('Edit own fb_feed entity'),
      'description' => t('Allows users to edit own fb_feed entity.'),
      'restrict access' => TRUE,
    ),
  );

  return $permissions;
}

/**
 * Implements hook_menu().
 */
function fb_feed_menu() {
  $items['admin/config/services/fb-feed'] = array(
    'title' => 'Facebook Feed Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('fb_feed_settings_form'),
    'access arguments' => array('administer content types'),
  );

  $items['admin/config/services/fb-feed/request'] = array(
    'title' => 'Facebook request page',
    'description' => 'Facebook request page.',
    'page callback' => 'fb_feed_request_page',
    'access callback' => TRUE,
  );

  return $items;
}

/**
 * Menu callback.
 */
function fb_feed_settings_form() {
  global $base_url;

  $form = array();

  $form['fb_feed_app_id'] = array(
    '#type' => 'textfield',
    '#title' => t('App ID'),
    '#default_value' => variable_get('fb_feed_app_id', FALSE),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['fb_feed_app_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('App Secret'),
    '#default_value' => variable_get('fb_feed_app_secret', FALSE),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['fb_feed_user_id'] = array(
    '#type' => 'textfield',
    '#title' => t('User/Page/Event ID'),
    '#default_value' => variable_get('fb_feed_user_id', FALSE),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['current_path'] = array(
    '#type' => 'hidden',
    '#value' => $base_url . '/' . $_GET['q'],
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#submit' => array('fb_feed_fetch_feeds'),
    '#value' => t('Fetch now'),
  );

  return system_settings_form($form);
}

/**
 * Submit callback.
 */
function fb_feed_fetch_feeds($form, &$form_state) {
  $app_id = !empty($form_state['values']['fb_feed_app_id']) ? variable_get('fb_feed_app_id', FALSE) : FALSE;
  $app_secret = !empty($form_state['values']['fb_feed_app_secret']) ? variable_get('fb_feed_app_secret', FALSE) : FALSE;
  $user_id = !empty($form_state['values']['fb_feed_user_id']) ? variable_get('fb_feed_user_id', FALSE) : FALSE;

  if ($app_id && $app_secret) {
    $_SESSION['fb_feed_app_id'] = $app_id;
    $_SESSION['fb_feed_app_secret'] = $app_secret;
    $_SESSION['fb_feed_user_id'] = $user_id;
    $_SESSION['fb_feed_return_path'] = $form_state['values']['current_path'];

    drupal_goto('admin/config/services/fb-feed/request');
  }
}

/**
 * Menu callback.
 */
function fb_feed_request_page() {
  global $base_url;
  $output = '';

  $app_id = !empty($_SESSION['fb_feed_app_id']) ? $_SESSION['fb_feed_app_id'] : variable_get('fb_feed_app_id', FALSE);
  $user_id = !empty($_SESSION['fb_feed_user_id']) ? $_SESSION['fb_feed_user_id'] : variable_get('fb_feed_user_id', FALSE);
  $app_secret = !empty($_SESSION['fb_feed_app_secret']) ? $_SESSION['fb_feed_app_secret'] : variable_get('fb_feed_app_secret', FALSE);
  $page_redirect = !empty($_SESSION['fb_feed_return_path']) ? $_SESSION['fb_feed_return_path'] : $base_url . '/' . $_GET['q'];

  if (empty($app_id) && empty($user_id) && empty($app_secret)) {
    return;
  }

  $redirect_uri = urlencode($base_url . '/admin/config/services/fb-feed/request');

  if (!empty($_SERVER["REDIRECT_QUERY_STRING"])) {
    $data = explode('=', $_SERVER["REDIRECT_QUERY_STRING"]);
    $code = $data[1];
  }

  if (empty($code)) {
    $path = "http://www.facebook.com/dialog/oauth?client_id={$app_id}&redirect_uri={$redirect_uri}&scope=manage_pages";
    drupal_goto($path);
  }
  $code = $code . '#_=_';

  $error = strstr($_SERVER["REDIRECT_QUERY_STRING"], 'error');
  if (empty($error) && $code != '#_=_') {
    $token_url = "https://graph.facebook.com/oauth/access_token?client_id=" . $app_id
      . "&redirect_uri=" . $redirect_uri
      . "&client_secret=" . $app_secret
      . "&code=" . $code;
    $access_token = file_get_contents($token_url);

    $feeds_path = "https://graph.facebook.com/{$user_id}/feed?{$access_token}&method=get";
    $feeds_request = drupal_http_request($feeds_path);
    if (!empty($feeds_request->data) && $feeds_request->code == 200) {
      _fb_feed_json_to_db($feeds_request->data, $user_id);

    }
    elseif ($feeds_request->code == 400) {
      drupal_set_message(t('Sorry. The service is not available at the moment.'), 'warning');
    }
  }
  else {
    drupal_set_message(t('Sorry. The service is not available at the moment. Please try again later.'), 'warning');
  }
  drupal_goto($page_redirect);

  return $output;
}

/**
 * Convert json data to array then write feeds to database.
 */
function _fb_feed_json_to_db($json_data, $user_id) {
  $data = drupal_json_decode($json_data);
  if (is_array($data['data']) && !empty($data['data'])) {
    $feeds = $data['data'];

    if (!empty($feeds) && is_array($feeds)) {
      $updated = $added = 0;
      foreach ($feeds as $feed) {
        // Variables.
        $type = $feed['type'];
        $picture = !empty($feed['picture']) ? $feed['picture'] : '';
        // Trick to download normal picture not small thumbnail.
        $picture = preg_replace('/(_s.)/', '_n.', $picture);

        if (!empty($feed['message'])) {
          $message = $feed['message'];
        }
        elseif (!empty($feed['story'])) {
          $message = $feed['story'];
        }
        else {
          $message = '';
        }

        $link = !empty($feed['link']) ? $feed['link'] : 'https://www.facebook.com/' . $user_id;
        $created = strtotime($feed['created_time']);

        // Check if entry already exist.
        $query = db_select('fb_feed_posts', 'fb');
        $query->fields('fb', array('id'));
        $query->condition('created', $created);
        $result = $query->execute()->fetchField();

        // Prepare object.
        $obj = new stdClass();
        $obj->type = $type;
        $obj->picture = $picture;
        $obj->message = $message;
        $obj->link = $link;
        $obj->created = $created;

        if (!empty($result)) {
          $obj->id = $result;
          drupal_write_record('fb_feed_posts', $obj, 'id');
          $updated++;
        }
        else {
          drupal_write_record('fb_feed_posts', $obj);
          $added++;
        }
      }
      watchdog('fb_feed', 'Fetch Facebook posts. Updated @updated, Created @created.', array(
        '@updated' => $updated,
        '@created' => $added,
      ));
    }
  }
}

/**
 * Implements hook_cron().
 */
function fb_feed_cron() {
  fb_feed_request_page();
}

/**
 * Implements hook_entity_info().
 */
function fb_feed_entity_info() {
  $entities = array();
  $entities['fb_feed_posts'] = array(
    'label' => t('Facebook Posts'),
    'entity class' => 'Entity',
    'controller class' => 'EntityAPIController',
    'views controller class' => 'EntityDefaultViewsController',
    'base table' => 'fb_feed_posts',
    'fieldable' => FALSE,
    'entity keys' => array(
      'id' => 'id',
      'label' => t('ID'),
    ),
    'bundle keys' => array(
      'bundle' => 'created',
    ),
    // Define bundles.
    'bundles' => array(),
    'uri callback' => 'entity_class_uri',
    'label callback' => 'fb_feed_get_label',
    'access callback' => 'fb_feed_access',
    'module' => 'fb_feed',
  );

  return $entities;
}

/**
 * Helper function. Calculate entity label.
 */
function fb_feed_get_label($entity_type, $entity) {
  return $entity_type->participant;
}

/**
 * Access callback for fb_feed.
 */
function fb_feed_access($op, $task, $account = NULL, $entity_type = NULL) {
  global $user;

  if (!isset($account)) {
    $account = $user;
  }
  switch ($op) {
    case 'create':
      return user_access('administer fb_feed entities', $account)
      || user_access('create fb_feed entities', $account);
    case 'view':
      return user_access('administer fb_feed entities', $account)
      || user_access('view fb_feed entities', $account);
    case 'edit':
      return user_access('administer fb_feed entities')
      || user_access('edit any fb_feed entities')
      || (user_access('edit own fb_feed entities') && ($task->uid == $account->uid));
  }
}

/**
 * Implements hook_views_api().
 */
function fb_feed_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'fb_feed'),
  );
}

/**
 * Implements hook_views_data_alter().
 */
function fb_feed_views_data_alter(&$data) {
  $data['fb_feed_posts']['created']['field']['handler'] = 'fb_feed_views_handler_field_date';
  $data['fb_feed_posts']['picture']['field']['handler'] = 'fb_feed_views_handler_field_picture';
  $data['fb_feed_posts']['message']['field']['handler'] = 'fb_feed_views_handler_field_message';
}
