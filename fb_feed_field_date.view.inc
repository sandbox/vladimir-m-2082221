<?php
/**
 * @file
 * Views handler.
 */

/**
 * A handler to provide proper displays for dates.
 */
class fb_feed_views_handler_field_date extends views_handler_field {

  /**
   * Define option_definition function.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['date_format'] = array('default' => 'small');
    $options['date_granularity'] = array('default' => 3);

    return $options;
  }

  /**
   * Define options_form function.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $date_formats = array();
    $date_types = system_get_date_types();
    foreach ($date_types as $key => $value) {
      $date_formats[$value['type']] = check_plain(t($value['title'] . ' format')) . ': ' . format_date(REQUEST_TIME, $value['type']);
    }
    $date_formats['time_ago'] = check_plain(t('Time ago'));

    $form['date_format'] = array(
      '#type' => 'select',
      '#title' => t('Date format'),
      '#options' => $date_formats,
      '#default_value' => isset($this->options['date_format']) ? $this->options['date_format'] : 'small',
    );

    $granularity = array(1, 2, 3, 4, 5, 6);

    $form['date_granularity'] = array(
      '#type' => 'select',
      '#title' => t('Date granularity'),
      '#options' => $granularity,
      '#default_value' => isset($this->options['date_granularity']) ? $this->options['date_granularity'] : '3',
    );
  }

  /**
   * Define render function.
   */
  function render($values) {
    $value = $this->get_value($values);
    $format = $this->options['date_format'];
    $granularity = $this->options['date_granularity'];

    if ($value) {
      if ($format == 'time_ago') {
        $date_formatted = format_interval((time() - $value) , $granularity) . t(' ago');
      }
      else {
        $date_formatted = format_date($value, $format);
      }

      return $date_formatted;
    }
  }
}
